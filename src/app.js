import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

import Login from './components/Login';

import { initialState, apiMiddleware, reducer } from './reducers/redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

let store = createStore(
  reducer,
  initialState,
  applyMiddleware(
    thunk,
    logger
  )
)

export default class Task7 extends Component {


  render() {
    return (
      <Provider store={store}>
        <Login />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('Task7', () => Task7);