import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/user';

mapStateToProps = (state) => {
    return {
        user: state.user
    };
}

mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            ...bindActionCreators(Actions, dispatch)
        }
    };
}

class Login extends Component {

    _onPress() {
        this.props.actions.login({
            username: "admin",
            password: "admin"
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={() => this._onPress()} >
                    <View style={{
                        width: 200,
                        height: 50,
                        backgroundColor: 'red'
                    }} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login)

