const PREFETCH_ITEMS = 5;

export const initialState = {
    item: null,
    user: null,
    next: []
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "LOGIN_SUCCESS":
            return {
                ...state,
                user: action.user
            };
        case "LOGIN_ERROR":
            return state;
        default:
            return state;
    }
}